# proj3-JSA
Vocabulary anagrams game for primary school English language learners (ELL)

Author: Jimmy Lam, jimmyl@uoregon.edu

## Overview

A simple anagram game designed for English-language learning students in elementary and middle school. Students are presented with a list of vocabulary words (taken from a text file) and an anagram. The anagram is a jumble of some number of vocabulary words, randomly chosen. Students attempt to type words that can be created from the jumble. When a matching word is typed, it is added to a list of solved words. 

The vocabulary word list is fixed for one invocation of the server, so multiple students connected to the same server will see the same vocabulary list but may  have different anagrams.

## Authors 

Initial version by M Young; Docker version added by R Durairajan; Final version completed by J Lam

## Extra Credit Implementation
The following features have been implemented, according to the last slide in the lecture slides on anagrams.
* Dynamic highlighting of partially built words: Whenever a user 
enters letters that can be found in the anagram, the first 
section of each word in the word list that matches the text entered 
into the text box will be highlighted in yellow.

* Grey-out used letters: All the letters in the anagram that 
have already been found will be in grey.

* Disable letters that aren't in the anagram: Whenever a user
enters a letter that is not in the anagram, the letter
will be immediately removed from the text box, and a
message will inform the user of the issue. 