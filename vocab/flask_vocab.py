"""
Flask web site with vocabulary matching game
(identify vocabulary words that can be made 
from a scrambled string)
"""

import flask
import logging

# Our own modules
from letterbag import LetterBag
from vocab import Vocab
from jumble import jumbled
import config

###
# Globals
###
app = flask.Flask(__name__)

CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY  # Should allow using session variables

#
# One shared 'Vocab' object, read-only after initialization,
# shared by all threads and instances.  Otherwise we would have to
# store it in the browser and transmit it on each request/response cycle,
# or else read it from the file on each request/responce cycle,
# neither of which would be suitable for responding keystroke by keystroke.

WORDS = Vocab(CONFIG.VOCAB)

# make copy of the word list
WORDLIST = [word for word in WORDS.as_list()]

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    """The main page of the application"""
    flask.g.vocab = WORDS.as_list()
    flask.session["target_count"] = min(
        len(flask.g.vocab), CONFIG.SUCCESS_AT_COUNT)
    flask.session["jumble"] = jumbled(
        flask.g.vocab, flask.session["target_count"])
    flask.session["matches"] = []
    app.logger.debug("Session variables have been set")
    assert flask.session["matches"] == []
    assert flask.session["target_count"] > 0
    app.logger.debug("At least one seems to be set correctly")
    return flask.render_template('vocab.html')


@app.route("/keep_going")
def keep_going():
    """
    After initial use of index, we keep the same scrambled
    word and try to get more matches
    """
    flask.g.vocab = WORDS.as_list()
    return flask.render_template('vocab.html')


@app.route("/success")
def success():
    app.logger.debug(">>>>> at redirect")
    return flask.render_template('success.html')

#######################
# Form handler.
# CIS 322 note:
#   You'll need to change this to a
#   a JSON request handler
#######################

def greyOutJumble(jumble, matches):
    """ Grey out the used letters in the anagram """
    matches = ''.join(matches)  # get single string of matches
    s = ''
    for letter in jumble:
        if letter in matches:
            s += '<span style="color:grey">{}</span>'.format(letter)
        else:
            s += letter
    return s

def highlight(text):
    """ Dynamically highlight the words in the list. returns
        a very long string that can be immediately used
        on the client side.
    """
    s = '<div class="row">'
    t_len = len(text)
    for i in range(len(WORDLIST)):
        word = WORDLIST[i]
        if word[:t_len] == text:
            word = '<span style="background-color:yellow">{}</span>{}'.format(
                    word[:t_len], word[t_len:])
        s += '<div class="col-xs-2 text-center"> {} </div>'.format(word)
        if (i + 1) % 3 == 0:
            s += '</div> <div class="row">'
    s += '</div>'
    return s

@app.route("/_check", methods=["POST", "GET"])
def check():
    """
    User has submitted the form with a word ('attempt')
    that should be formed from the jumble and on the
    vocabulary list.  We respond depending on whether
    the word is on the vocab list (therefore correctly spelled),
    made only from the jumble letters, and not a word they
    already found.
    """
    app.logger.debug("Entering check")

    text = flask.request.args.get("text", type=str)
    flash_msg = ''  # ajax implementation of flask.flash
    is_done = 0  # whether to reload the success page or not
    clear_input = 0  # to clear input in text box

    jumble = flask.session["jumble"]
    matches = flask.session.get("matches", [])  # Default to empty list

    # Is it good?
    in_jumble = LetterBag(jumble).contains(text)
    matched = WORDS.has(text)

    # Respond appropriately
    if matched and in_jumble and not (text in matches):
        # Cool, they found a new word
        matches.append(text)
        flask.session["matches"] = matches
        clear_input = 1
    elif len(text) > 0 and text[-1] not in jumble:
        flash_msg = '"{}" is not in the anagram'.format(text[-1])
    elif text in matches:
        flash_msg = "You already found {}".format(text)
    elif not matched:
        flash_msg = "{} isn't in the list of words".format(text)
    elif not in_jumble:
        flash_msg = '"{}" can\'t be made from the letters {}'.format(text, jumble)
    else:
        app.logger.debug("This case shouldn't happen!")
        assert False  # Raises AssertionError

    # Solved enough, or keep going?
    if len(matches) >= flask.session["target_count"]:
        app.logger.debug("--- SHOULD BE REDIRECTED ---")
        is_done = 1

    # create the JSON object
    rslt = {"thematches": matches,  # The words found from the anagram
            "flash_msg": flash_msg, # error message to flash in red
            "is_done": is_done,     # 1 if all the words are found, 0 if not
            "jumble_formatted": greyOutJumble(jumble, matches),  # used letters are greyed out
            "jumble_raw": jumble,   # the original jumble string (no formatting)
            "word_list": highlight(text),  # the list of words with dynamic highlighting
            "clear_input": clear_input}
    
    return flask.jsonify(result=rslt)


###############
# AJAX request handlers
#   These return JSON, rather than rendering pages.
###############


@app.route("/_example")
def example():
    """
    Example ajax request handler
    """
    app.logger.debug("Got a JSON request")
    rslt = {"key": "value"}
    return flask.jsonify(result=rslt)


#################
# Functions used within the templates
#################

@app.template_filter('filt')
def format_filt(something):
    """
    Example of a filter that can be used within
    the Jinja2 code
    """
    return "Not what you asked for"

###################
#   Error handlers
###################


@app.errorhandler(404)
def error_404(e):
    app.logger.warning("++ 404 error: {}".format(e))
    return flask.render_template('404.html'), 404


@app.errorhandler(500)
def error_500(e):
    app.logger.warning("++ 500 error: {}".format(e))
    assert not True  # I want to invoke the debugger
    return flask.render_template('500.html'), 500


@app.errorhandler(403)
def error_403(e):
    app.logger.warning("++ 403 error: {}".format(e))
    return flask.render_template('403.html'), 403


####

if __name__ == "__main__":
    if CONFIG.DEBUG:
        app.debug = True
        app.logger.setLevel(logging.DEBUG)
        app.logger.info(
            "Opening for global access on port {}".format(CONFIG.PORT))
        app.run(port=CONFIG.PORT, host="0.0.0.0")
